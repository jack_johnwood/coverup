# Deployment Process:



## Env setup
- Set up a mongodb server and write its address to server/.env in a variable named `MONGO_URI`.
- The default server port is `3000` but you can change it using `PORT` variable.
- Set `NODE_ENV` variable to `production`



## Docker and Nginx setup
Setup nginx and a reverse proxy to whatever port you chose in the last step.
You'll need to set up a domain and connect it to your server, ssl connection is required.



## Trello Powerup setup
After that you can setup your trello powerup.