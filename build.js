const fs = require('fs');
const uglyfyJS = require('uglify-es');
const program = require('commander');
const path = require('path');
const browserify = require("browserify");
program
    .option('-p, --prebuild', 'pre building it')
    .option('-b, --build', 'building it')
    .option('-d, --directory <directory>', 'building it with the specified directory')

program.parse(process.argv);

if (program.build) {
    build({
        dir: program.directory,
        src: 'lib/public/js',
    });

} else if (program.prebuild) {
    preBuild({
        dir: program.directory,
        src: 'lib/public/js',
    });
}


function preBuild(opts) {

    var dir = opts.dir;

    if (!dir) {
        dir = `${__dirname}/${opts.src}`;
    }

    fs.readdir(dir, function (err, files) {
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }

        files.forEach(function (filePath) {
            var fileName = (path.basename(filePath).split('.'))[0];
            if (filePath === `${fileName}.js` && fileName != '') {
                const newPath = `${dir}/${fileName}.build.js`;

                browserify(`${dir}/${filePath}`)
                    .transform('babelify', { presets: ['@babel/preset-env'] })
                    .bundle()
                    .pipe(fs.createWriteStream(newPath));
                console.log(`${fileName} built.`);
            }
        }
        )
    });
}

function build(opts) {

    var dir = opts.dir;

    if (!dir) {
        dir = `${__dirname}/${opts.src}`;
    }

    fs.readdir(dir, function (err, files) {
        if (err) {
            return console.log('Unable to scan directory: ' + err);
        }

        files.forEach(function (filePath) {
            const fileName = (path.basename(filePath).split('.'))[0];
            const newPath = `${dir}/${fileName}.min.js`;
            const origPath = `${dir}/${fileName}.build.js`;

            try {
                if (filePath === `${fileName}.build.js` && fileName != '') {
                    const content = fs.readFileSync(origPath).toString();
                    fs.writeFileSync(newPath, uglyfyJS.minify(content).code);
                    console.log(`${fileName} built.`);
                }

            } catch (e) {
                return e;
            }
        });
    });
}
