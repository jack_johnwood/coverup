'use strict';

window.TrelloPowerUp.initialize({
    'list-actions': function (t) {
        return t.cards('idList')
            .then(function (cards) {
                let cardsCount = 0;
                const listId = t.getContext().list;
                for (let card of cards) {
                    if (card.idList === listId) {
                        cardsCount++;
                    }
                }
                return [{
                    text: 'Hill Chart',
                    callback: function (t) {
                        t.modal({
                            url: `/single/${cardsCount}/`,
                            accentColor: '#ffb400'
                        })
                    }
                }]
            });
    },
    'board-buttons': function (t, opts) {
        return t.lists('all')
            .then(function (lists) {
                let count = 0;
                for (let item of lists) {
                    if (item.cards.length === 0) {
                        count++;
                    }
                }
                return [{
                    text: 'Hill Charts',
                    callback: function (t) {
                        t.modal({
                            url: `/overview/${lists.length - count}/`,
                            fullscreen: true
                        })
                    }
                }]
            });
    }
});


