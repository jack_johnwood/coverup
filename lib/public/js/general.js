'use strict';

var t = TrelloPowerUp.iframe();
var cardIds = [];
var arg = [];
var hill = [];
var listNames = [];

t.render(function () {
    return t.lists('all')
        .then(function (lists) {
            for (let list of lists) {
                for (let card of list.cards) {
                    cardIds.push(card.id)

                    arg.push({
                        'listName': list.name,
                        'card': {
                            'cardName': card.name,
                        }
                    });
                }
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/load/');
            xhr.setRequestHeader('Content-Type', 'application/json');

            xhr.send(JSON.stringify({
                cardIds
            }))

            xhr.onload = async () => {
                let i = 0;
                let posX = await JSON.parse(xhr.response);

                for (var cards of arg) {
                    cards['card']['posX'] = posX.posX[i]
                    i++;
                }

                let cardsList = arg;

                function getY(x) {
                    return 100 * Math.sin(x / 5 / Math.PI - Math.PI / 2) + 100;
                }


                for (let x = 0; x < 101; x++) {
                    hill.push([x, getY(x)]);
                }


                for (let i = 0; i < cardsList.length;) {
                    let item = cardsList[i];
                    let listName = item.listName;

                    if (!(listNames.includes(listName))) {
                        listNames.push(listName);

                        var chart = Highcharts.chart(`${listNames.length}`, {
                            credits: {
                                enabled: false
                            },
                            yAxis: {
                                visible: false
                            },
                            title: {
                                text: listName,
                            },
                            legend: {
                                layout: 'vertical',
                                verticalAlign: 'middle',
                                marginBottom: 100,
                                maxHeight: 83,
                            },
                            plotOptions: {
                                spline: {
                                    marginBottom: 100,
                                    marker: {
                                        enabled: false
                                    }
                                },
                                scatter: {
                                    marginBottom: 100,
                                    marker: {
                                        radius: 8
                                    },
                                    dataLabels: {
                                        enabled: false
                                    },
                                    enableMouseTracking: false
                                },
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                }
                            },

                            series: [{
                                type: 'spline',
                                marginBottom: 100,
                                data: hill,
                                showInLegend: false,
                                enableMouseTracking: false
                            }],

                            responsive: {
                                rules: [{
                                    condition: {
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'vertical',
                                            align: 'center',
                                            verticalAlign: 'bottom',
                                        }
                                    }
                                }]
                            }
                        });
                    }
                    chart.addSeries({
                        type: 'scatter',
                        name: item.card.cardName,
                        data: [{
                            x: item.card.posX,
                            y: getY(item.card.posX)
                        }]
                    });
                    i++;
                }
            }
        })

});