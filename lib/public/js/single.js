'use strict';

var t = window.TrelloPowerUp.iframe();
t.render(function () {
    return t.list('all')
        .then(function (list) {
            const listName = list.name;

            function getY(x) {
                return 100 * Math.sin(x / 5 / Math.PI - Math.PI / 2) + 100;
            }

            var hill = [];

            for (let x = 0; x < 101; x++) {
                hill.push([x, getY(x)]);
            }

            var chart = Highcharts.chart('container', {
                credits: {
                    enabled: false
                },
                yAxis: {
                    visible: false
                },
                title: {
                    text: listName,
                },
                legend: {
                    layout: 'vertical',
                    verticalAlign: 'middle',
                    maxHeight: 83,

                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    },
                    scatter: {
                        marker: {
                            radius: 8
                        },
                        dataLabels: {
                            enabled: false
                        },
                        enableMouseTracking: false
                    },
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                    }
                },

                series: [{
                    type: 'spline',
                    data: hill,
                    showInLegend: false,
                    enableMouseTracking: false
                }],

                responsive: {
                    rules: [{
                        condition: {
                        },
                        chartOptions: {
                            legend: {
                                layout: 'vertical',
                                align: 'center',
                                verticalAlign: 'bottom',
                            }
                        }
                    }]
                }
            });

            var flexItem = document.getElementsByClassName('flex-item');
            var count = 0;
            var series = chart.series;
            let cardsData = [];

            for (const card of list.cards) {
                const cardNameTag = document.createElement('p');
                cardNameTag.innerHTML = card.name;
                flexItem[count].append(cardNameTag);

                chart.addSeries({
                    type: 'scatter',
                    name: card.name,
                });

                cardsData.push({
                    id: card.id,
                    name: card.name
                })
                count++;
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', `/stat/checkSave/${list.id}/`);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify({
                cardsData
            }))

            xhr.onload = async () => {
                const loadedCards = await JSON.parse(xhr.response);
                count = 0;

                for (let set of series) {
                    if (set.name === 'Series 1')
                        continue;
                    if (set.name === list.cards[count].name) {
                        set.setData([{
                            x: loadedCards[count].posX,
                            y: getY(loadedCards[count].posX)
                        }], true, { duration: 2 });
                    }

                    const slider = document.createElement('input');

                    slider.id = loadedCards[count].id;
                    slider.name = list.cards[count].name
                    slider.className = 'slider'
                    slider.type = 'range';
                    slider.min = 0; slider.max = 100;
                    slider.value = loadedCards[count].posX;
                    flexItem[count].append(slider);
                    count++;
                }
                t.sizeTo('#modal');



                count = 0;
                const sliders = document.getElementsByClassName('slider');

                for (const slider of sliders) {
                    slider.oninput = () => {
                        for (let set of series) {
                            if (set.name === 'Series 1')
                                continue;

                            if (set.name === slider.name) {
                                set.setData([{
                                    x: slider.value,
                                    y: getY(slider.value)
                                }], true, { duration: 2 });
                            }
                            count++;
                        }
                    }

                    slider.onchange = () => {
                        var xhr = new XMLHttpRequest();
                        xhr.open('PUT', '/scope/' + slider.id);
                        xhr.setRequestHeader('Content-Type', 'application/json');
                        xhr.send(JSON.stringify({
                            posX: slider.value,
                        }))
                    }
                }
            }
        });
});