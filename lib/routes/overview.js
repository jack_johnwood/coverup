'use strict';

module.exports = {
    method: 'GET',
    path: '/overview/{count}/',
    options: {
        handler: async (request, h) => {
            const { count } = request.params;
            let countList = [];

            for (let i = 0; countList.length < count; i++) {
                countList.push(i + 1)
            }

            return h.view('general', {
                countList
            })
        }
    }
};
