'use strict';

module.exports = {
    method: 'GET',
    path: '/public/{p*}',
    handler: {
        directory: {
            path: 'public'
        }
    }
};