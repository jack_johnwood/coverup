'use strict';

const Boom = require('@hapi/boom');

module.exports = [{
    method: 'PUT',
    path: '/scope/{id}',
    options: {
        handler: async (request, h) => {
            const { Scope } = request.server.app.models;
            const { id } = request.params;
            const { posX } = request.payload;
            try {
                const card = await Scope.findOneAndUpdate({ cardId: id }, { posX: posX });
                return {
                    cardId: card.cardId,
                    posX: posX,
                }
            } catch {
                return Boom.badData();
            }

        }

    }
},
{
    method: 'POST',
    path: '/stat/checkSave/{listId}/',
    options: {
        handler: async (request, h) => {

            const { cardsData } = request.payload;
            const { listId } = request.params;
            const { Scope } = request.server.app.models;
            let cards = [];

            ////////////////////////// adding/getting the card from database ////////////////////////////////////
            try {
                for (let card of cardsData) {

                    let loadedCard = await Scope.find({ cardId: card.id });

                    if (loadedCard.length === 0) {
                        const newCard = new Scope({
                            listId: listId,
                            cardId: card.id,
                            posX: 0,
                            cardName: card.name
                        });
                        await newCard.save()
                        cards.push({
                            posX: 0,
                            id: card.id,
                            name: card.name
                        })
                    }
                    else {
                        cards.push({
                            posX: loadedCard[0].posX,
                            id: loadedCard[0].cardId,
                            name: card.name
                        })
                    }
                }
            } catch (e) {
                return Boom.boomify(e);
            }
            /////////////////////////////////////////////////////////////////////
            return cards
        }
    }
}]
