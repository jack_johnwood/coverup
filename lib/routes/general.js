'use strict';
const Boom = require('@hapi/boom');

module.exports = {
    method: 'POST',
    path: '/load/',
    options: {
        handler: async (request, h) => {
            const { Scope } = request.server.app.models;
            const cardIds = request.payload;
            let posX = [];

            try {
                for (let cardId of cardIds.cardIds) {
                    let card = (await Scope.find({ cardId: cardId }));

                    if (card[0] == null)
                        posX.push(0)
                    else
                        posX.push(card[0].posX)

                }
                return {
                    posX
                }
            } catch (e) {
                return Boom.boomify(e);
            }
        }
    }
};
