'use strict';

module.exports = {
    method: 'GET',
    path: '/',
    handler: {
        view: {
            template: 'main'
        }
    }
};
