'use strict';

const Boom = require('@hapi/boom');
const Wreck = require('@hapi/wreck');

module.exports = {
    method: 'GET',
    path: '/single/{cardsCount}/',
    handler: async (request, h) => {

        const { cardsCount } = request.params;

        let countCards = [];

        for (let i = 0; countCards.length < cardsCount; i++) {
            countCards.push(i)
        }

        return h.view('single', {
            countCards: countCards,
        })

    }
};