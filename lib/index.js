'use strict';

const HauteCouture = require('haute-couture');
const Package = require('../package.json');
const Mongoose = require('mongoose');

exports.plugin = {
    pkg: Package,
    register: async (server, options) => {

        // Custom plugin code can go here
        server.app.connection = Mongoose.createConnection(options.mongo.uri, options.mongo.options);
        await HauteCouture.using()(server, options);
    }
};
