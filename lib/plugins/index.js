'use strict';

module.exports = {
    plugins: [
        {
            plugin: require('@hapi/inert'),
        },
        {
            plugin: require('@hapi/vision')
        }
    ]
};
