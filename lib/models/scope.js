'use strict';

const Mongoose = require('mongoose');

const scopeSchema = new Mongoose.Schema({
    listId: { type: String, required: true },
    cardId: { type: String, required: true },
    posX: { type: Number, required: true },
})

module.exports = {
    name: 'Scope',
    schema: scopeSchema
};