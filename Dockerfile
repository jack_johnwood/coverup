# Source image with node 12
FROM node:12-buster

RUN mkdir /app/
WORKDIR /app/

# Install and upgrade pip and uwsgi
RUN npm i -g pm2

# Set shell and entrypoint
SHELL ["/bin/sh", "-c"]
ENTRYPOINT []

# Add package.json to cache npm i
ADD package.json .
ADD package-lock.json .

# Install dependencies
RUN npm i

# Copy code to container
ADD . .

# Run prebuild and build scripts
RUN npm run prebuild
RUN npm run build

# Run code using pm2
CMD pm2 start server --name coverup --no-daemon